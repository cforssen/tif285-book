# TIF285 Learning from data
This repository contains the lecture notes for the Chalmers course "Learning from data" in the Physics MSc program.

The course website can be accessed at: https://cforssen.gitlab.io/tif285-book/
